package entity.photo

import com.sun.imageio.plugins.jpeg.JPEG
import java.util.*

data class Photo (val date : Date, val file : JPEG)