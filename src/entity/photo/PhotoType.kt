package entity.photo

enum class PhotoType {
    FRONTAL, REAR
}