package entity.phone

import com.sun.imageio.plugins.jpeg.JPEG
import entity.app.App
import entity.message.Message
import entity.number.PhoneNumber
import entity.photo.Photo
import entity.photo.PhotoType
import entity.producer.Producer
import entity.producer.Samsung
import pattern.command.Command
import pattern.data.DefaultData
import java.util.*

/**
 * Created by alexandr on 07.09.17.
 */
abstract class BasePhone(final override var name: String? = "No Name Yet", final override var weight: Int? = 100, final override var producer: Producer? = Samsung()) : Phone {
    final override val apps: MutableList<App> = mutableListOf()
    final override val defaultData: DefaultData = DefaultData(9000, JPEG(), "MyPhone")
    override val uid: String = UUID.randomUUID().toString()


    override fun call(number: PhoneNumber) {
        println("Calling to {$number}")
    }

    override fun runCommand(command: Command){
        command.execute()
    }

    override fun makePhoto(type: PhotoType): Photo {
        println("making photo with $type camera...")
        return Photo(Date(), JPEG())
    }

    override fun sendMessage(number: PhoneNumber, content: Message) {
        println("sending message {$content.msg} ")
    }

    override fun installApp(app: App): Boolean {
        return if (app.os != this.os || this.apps.contains(app))
            false
        else {
            app.install(this)
            this.apps.add(app)
            true
        }
    }

    override fun toString(): String {
        return "---\n" +
                "Producers: ${producer?.name ?: "No entity.producer specified"} \n" +
                "Model: ${name ?: "No Name Yet"} \n" +
                "Weight: ${weight ?: 0} \n" +
                "DefaultData : $defaultData\n" +
                "---"
    }

}

