package entity.phone

import entity.app.App
import entity.files.Element
import entity.message.Message
import entity.number.PhoneNumber
import entity.os.OS
import entity.photo.Photo
import entity.photo.PhotoType
import entity.producer.Producer
import pattern.command.Command
import pattern.data.DefaultData
import pattern.proxy.EasyPhone

/**
 * Created by alexandr on 07.09.17.
 */
interface Phone : EasyPhone,Cloneable{
    fun installApp(app : App) : Boolean
    fun makePhoto(type : PhotoType) : Photo
    fun sendMessage(number: PhoneNumber, content : Message)
    fun runCommand(command: Command)
    public override fun clone() : Any
    val fileSystem : Element
    val producer: Producer?
    val weight: Int?
    val name: String?
    val os: OS?
    val apps : List<App>
    val defaultData: DefaultData
    val uid: String
}
