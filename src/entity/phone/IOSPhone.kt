package entity.phone

import entity.files.Element
import entity.files.Folder
import entity.os.OS
import entity.producer.Apple

class IOSPhone : BasePhone {
    override val os = OS.IOS
    override val fileSystem: Element = Folder("_main")

    constructor(): super()

    constructor(name: String?, weight: Int? = 165) : super(name, weight, Apple())

    override fun clone(): Any {
        return IOSPhone(name,weight)
    }


}