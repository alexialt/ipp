package entity.phone

import entity.files.Element
import entity.files.Folder
import entity.os.OS
import entity.producer.Producer
import entity.producer.Samsung
import pattern.mediator.NotificationSystem

class AndroidPhone : BasePhone {
    override val os = OS.ANDROID
    override val fileSystem: Element = Folder("/")
    var notificationSystem: NotificationSystem? = null

    constructor() : super()

    constructor(name: String? = "Android Device", weight: Int? = 150, producer: Producer? = Samsung()) : super(name, weight, producer)

    override fun clone(): Any {
        return AndroidPhone(name, weight, producer)
    }

    fun checkNotifications(): Boolean {
        notificationSystem?.apply {
            val notifications = this.getNotification(uid)
            println("$name got :")
            if (notifications.isNotEmpty()) {
                println(notifications.joinToString ("\n") { it })
                return true
            } else {
                println("no messages")
                return false
            }
        }
        println("no notification system connected")
        return false
    }
}