package entity.phone

import com.sun.imageio.plugins.jpeg.JPEG
import javax.swing.text.html.HTML

class SymbianPhone : OldPhone {
    override fun makeCall(number: String, isAnonym: Boolean) {
        println("calling $number, anonym = $isAnonym")
    }

    override fun sendSMS(number: String, msg: String) {
        println("sending message $msg to $number")
    }

    override fun sendMMS(number: String, msg: HTML) {
        println("sending MMS message to number")
    }

    override fun makePhoto(): JPEG {
        println("making photo with symbian photo camera")
        return JPEG()
    }

    override val model: String
        get() = "J5732"
}