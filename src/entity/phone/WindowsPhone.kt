package entity.phone

import entity.files.Element
import entity.files.Folder
import entity.os.OS
import entity.producer.Microsoft
import entity.producer.Producer

class WindowsPhone : BasePhone {
    override val os = OS.WINDOWS
    override val fileSystem: Element = Folder("system")

    constructor():super()

    constructor(name: String?, weight: Int? = 100, producer: Producer? = Microsoft()) : super(name, weight, producer)

    override fun clone(): Any {
        return WindowsPhone(name,weight,producer)
    }
}