package entity.phone

import com.sun.imageio.plugins.jpeg.JPEG
import javax.swing.text.html.HTML

interface OldPhone {
    fun makeCall(number: String, isAnonym : Boolean)
    fun sendSMS(number: String, msg : String)
    fun sendMMS(number: String, msg : HTML)
    fun makePhoto() : JPEG
    val model : String
}