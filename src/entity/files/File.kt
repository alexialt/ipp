package entity.files

class File(override val name: String, override val size : Double) : Element {
    override val elements: List<Element> = listOf()
    override fun add(element: Element) {
        println("cannot create elements inside a file")
    }

    override fun remove(element: Element) {
        println("cannot remove elements inside folder")
    }

    override fun toString(): String {
        return "$name ($size MB)"
    }
}