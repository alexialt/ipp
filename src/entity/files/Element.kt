package entity.files

interface Element {
    val elements : List<Element>
    val name : String
    val size : Double
    fun add(element : Element)
    fun remove(element : Element)
    override fun toString() : String
}