package entity.files

class Folder(override val name: String) : Element {
    override val elements: MutableList<Element> = mutableListOf()
    override val size: Double
        get() {
            return if (elements.size > 0)
                elements.sumByDouble { it.size }
            else
                0.0
        }

    override fun add(element: Element) {
        if (!elements.contains(element))
            elements.add(element)
    }

    override fun remove(element: Element) {
        elements.remove(element)
    }

    override fun toString(): String {
        return "$name ($size MB)" + if (elements.isNotEmpty())
            " $elements"
        else
            ""
    }
}