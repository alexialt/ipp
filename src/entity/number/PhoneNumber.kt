package entity.number

data class PhoneNumber(val countryCode : String, val cityCode : String, val number : String)