package entity.os

enum class OS {
    ANDROID, IOS, WINDOWS
}