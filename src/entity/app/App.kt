package entity.app

import entity.os.OS
import entity.phone.Phone

interface App {
    val name : String
    val size : Float
    val os : OS
    val price : Int
    fun install(phone : Phone)
    fun run()
    fun stop()

}