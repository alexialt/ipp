package entity.app

import entity.os.OS
import entity.phone.Phone

class WindowsMarket : MarketApp() {
    override val name: String = "Windows Market"
    override val size: Float = 14.25f
    override val os: OS = OS.WINDOWS
    override val price: Int = 0
    override fun run() {
        println("started WindowsMarket App")
    }

    override fun stop() {
        println("stopped WindowsMarket App")
    }
}