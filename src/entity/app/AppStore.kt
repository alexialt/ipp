package entity.app

import entity.os.OS
import entity.phone.Phone

class AppStore : MarketApp() {
    override val name: String = "App Store"
    override val size: Float = 12.75f
    override val os: OS = OS.IOS
    override val price: Int = 0
    override fun install(phone: Phone) {
        println("$name installed on ${phone.name}")
    }

    override fun run() {
        println("started AppStore App")
    }

    override fun stop() {
        println("stopped AppStore App")
    }
}
