package entity.app

import entity.phone.Phone

abstract class MarketApp : App {


    override fun install(phone: Phone) {
        println("$name installed on ${phone.name}")
    }
    override fun toString(): String {
        return "$name ($size MB, os = $os, price = $price $)"
    }
}