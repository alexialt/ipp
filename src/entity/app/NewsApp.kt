package entity.app

import entity.os.OS
import entity.phone.Phone
import entity.server.Listener
import entity.server.Publisher

class NewsApp (val server : Publisher<String>): App, Listener<String> {
    @Volatile
    private var phone : Phone? = null
    override fun onEventReceived(event: String) {
        println("received news : $event on ${phone?.name}")
    }

    override fun install(phone: Phone) {
        if (this.phone != null)
            throw RuntimeException("$name is already installed on a device!")
        this.phone = phone
    }

    override val name: String
        get() = "NewsApp"
    override val size: Float
        get() = 3.5f
    override val os: OS
        get() = OS.ANDROID
    override val price: Int
        get() = 0

    @Synchronized
    override fun run() {
        if (this.phone == null)
            throw RuntimeException("$name is not installed!")
        println("started NewsApp on ${phone?.name}")
        server.subscribe(this)
    }

    @Synchronized
    override fun stop() {
        if (this.phone == null)
            throw RuntimeException("$name is not installed!")
        println("stopped NewsApp on ${phone?.name}")
        server.unsubscribe(this)
    }
}