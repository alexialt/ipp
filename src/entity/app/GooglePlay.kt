package entity.app

import entity.os.OS
import entity.phone.Phone

class GooglePlay : MarketApp() {
    override val name: String = "Google Play"
    override val size: Float = 15.5f
    override val os: OS = OS.ANDROID
    override val price: Int = 0

    override fun install(phone: Phone) {
        println("$name installed on ${phone.name}")
    }

    override fun run() {
        println("started GooglePlay App")
    }

    override fun stop() {
        println("stopped WindowsMarket App")
    }
}