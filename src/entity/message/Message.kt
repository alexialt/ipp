package entity.message

import com.sun.imageio.plugins.jpeg.JPEG
import java.util.*

data class Message(val date : Date, val msg : String, val photo : JPEG?)