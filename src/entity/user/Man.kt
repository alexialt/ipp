package entity.user

import entity.phone.Phone
import entity.photo.Photo
import entity.photo.PhotoType

class Man(override val name: String) : User {
    override val sex: Sex = Sex.MALE
    override var phone: Phone? = null

    override fun takeSelfie(): Photo? {
        return if (phone == null){
            println("$name not having a phone... doing nothing...")
            null
        } else {
            val phoneName = phone?.name
            println("$name having a phone and tries to take a selfie with {$phoneName}")
            phone?.makePhoto(PhotoType.FRONTAL)
        }
    }
}