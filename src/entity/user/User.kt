package entity.user

import entity.phone.Phone
import entity.photo.Photo

interface User {
    val name: String
    val sex: Sex
    var phone: Phone?
    fun takeSelfie() : Photo?
}