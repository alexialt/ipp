package entity.user

import entity.number.PhoneNumber
import entity.phone.Phone
import entity.photo.Photo
import entity.photo.PhotoType
import java.util.*

class Woman(override val name: String) : User {
    override val sex: Sex = Sex.FEMALE
    override var phone: Phone? = null

    override fun takeSelfie(): Photo? {
        println("$name having a phone to take a selfie...")
        if (phone == null){
            println("$name not having a phone...")
            for (i in 1..5)
                println("crying...")
            return null
        } else {
            val rand = Random()
            for (i in 1..30) {
                val phoneName = phone?.name
                println("making selfie with $phoneName")
                val photo = phone?.makePhoto(PhotoType.FRONTAL)
                if (Math.abs(rand.nextInt()) % 3 == 0)
                {
                    println("$name got a good selfie. posting the photo...")
                    return photo
                } else
                    println("$name got bad selfie...")
            }
            println("$name cannot make good selfie today... crying... and posting nothing...")
            return null
        }
    }
}