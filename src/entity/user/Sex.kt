package entity.user

enum class Sex {
    MALE, FEMALE
}