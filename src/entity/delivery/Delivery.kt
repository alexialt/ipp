package entity.delivery

/**
 * Created by alexandr on 07.09.17.
 */
interface Delivery {
    val name: String
    val periodInDays: Int
    val price: Int
}
