package entity.producer

class Google : Producer {
    override val name: String
        get() = "Google"
    override val year: Int
        get() = 2007
}