package entity.producer

class Samsung : Producer {
    override val name: String
        get() = "Samsung"
    override val year: Int
        get() = 1938
}