package entity.producer

class Microsoft : Producer {
    override val name: String
        get() = "Microsoft"
    override val year: Int
        get() = 1981
}