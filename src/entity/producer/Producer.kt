package entity.producer

/**
 * Created by alexandr on 07.09.17.
 */
interface Producer {
    val name: String
    val year: Int
}
