package entity.producer

/**
 * Created by alexandr on 07.09.17.
 */
class Apple : Producer {
    override val name: String
        get() = "Apple"

    override val year: Int
        get() = 1974
}
