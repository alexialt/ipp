package entity.server

import java.util.*

class NewsPublisher : Publisher<String>, Server<String> {
    private val subscribers = Collections.synchronizedSet(mutableSetOf<Listener<String>>())
    override fun subscribe(subscriber: Listener<String>) {
        subscribers.add(subscriber)
    }

    override fun unsubscribe(subscriber: Listener<String>) {
        subscribers.remove(subscriber)
    }

    override fun notifyUsers(msg: String) {
        subscribers.forEach { it.onEventReceived(msg) }
    }
}