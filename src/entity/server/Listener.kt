package entity.server

interface Listener<T> {
    fun onEventReceived(event : T)
}