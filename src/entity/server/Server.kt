package entity.server

interface Server<T> {
    fun notifyUsers(msg: T)
}