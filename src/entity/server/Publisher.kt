package entity.server

interface Publisher<T> {
    fun subscribe(subscriber : Listener<T>)
    fun unsubscribe(subscriber : Listener<T>)
}