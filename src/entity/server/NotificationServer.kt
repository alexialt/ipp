package entity.server

import pattern.mediator.NotificationSystem

class NotificationServer(val notificationSystem: NotificationSystem) : Server<String>{
    override fun notifyUsers(msg: String) {
        notificationSystem.pushNotification(msg)
    }
}