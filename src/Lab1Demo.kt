import entity.os.OS
import pattern.builder.*
import pattern.factory.AbstractPhoneFactory
import pattern.method.PhoneFactory
import pattern.prototype.ConcretePhoneProtoFactory
import pattern.prototype.PhoneProtoFactory
import pattern.singleton.GlobalStats

/**
 * Created by alexandr on 07.09.17.
 */

fun main(args: Array<String>) {
    println("*BUILDER FACTORY*")
    builderDemo()
    println()
    println("*ABSTRACT FACTORY PATTERN*")
    abstractFactoryDemo()
    println()
    println("*FACTORY METHOD DEMO*")
    println()
    factoryMethodDemo()
    println()
    println("*PROTOTYPE PATTERN*")
    prototypeDemo()
    println()
    println("*SINGLETON PATTERN*")
    singletonDemo()
}

fun factoryMethodDemo(){
    println("CREATING IOS DEVICE")
    println(PhoneFactory.createPhone(OS.IOS))
    println("CREATING ANDROID DEVICE")
    println(PhoneFactory.createPhone(OS.ANDROID))
    println("CREATING WINDOWS DEVICE")
    println(PhoneFactory.createPhone(OS.WINDOWS))
}

fun singletonDemo() {
    println(GlobalStats.getInstance())
    println("SELLING 4 iPhones...")
    for (i in 1..4)
        GlobalStats.getInstance()?.sellIOS()
    println(GlobalStats.getInstance())
    println("SELLING 20 Androids...")
    for (i in 1..20)
        GlobalStats.getInstance()?.sellAndroid()
    println(GlobalStats.getInstance())
    println("SELLING 1 Windows...")
    GlobalStats.getInstance()?.sellWindows()
    println()
    println(GlobalStats.getInstance())
}

fun prototypeDemo() {
    val protoFactory : PhoneProtoFactory = ConcretePhoneProtoFactory()
    println("USING PROTOTYPE TO GET ANDROID PHONE")
    println(protoFactory.getPhone(OS.ANDROID))
    println("USING PROTOTYPE TO GET IOS PHONE")
    println(protoFactory.getPhone(OS.IOS))
    println("USING PROTOTYPE TO GET WINDOWS PHONE")
    println(protoFactory.getPhone(OS.WINDOWS))
}

fun abstractFactoryDemo() {
    println("USING ANDROID FACTORY")
    var factory : AbstractPhoneFactory = AbstractPhoneFactory.getFactory(OS.ANDROID)
    println(factory.createPhone())
    println(factory.createMarketApp())
    println()
    println("USING WINDOWS FACTORY")
    factory = AbstractPhoneFactory.getFactory(OS.WINDOWS)
    println(factory.createPhone())
    println(factory.createMarketApp())
    println()
    println("USING IOS FACTORY...")
    factory = AbstractPhoneFactory.getFactory(OS.IOS)
    println(factory.createPhone())
    println(factory.createMarketApp())
}

fun builderDemo() {
    println("BUILDING PHONES USING BUILDER...")
    var phoneBuilder: PhoneBuilder = AppleBuilder()
    val iPhone6s = phoneBuilder
            .createNewPhone()
            .buildMarketApp()
            .buildName("iPhone 6s")
            .buildWeight(128)
            .build()
    println(iPhone6s)
    phoneBuilder = SamsungBuilder()
    val galaxyJ5 = phoneBuilder
            .createNewPhone()
            .buildName("Galaxy J5")
            .buildMarketApp()
            .buildWeight(196)
            .build()
    println(galaxyJ5)
    phoneBuilder = MicrosoftBuilder()
    val lumiaX2 = phoneBuilder
            .createNewPhone()
            .buildName("Lumia X2")
            .buildMarketApp()
            .buildWeight(196)
            .build()
    println(lumiaX2)
}
