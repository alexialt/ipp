import entity.app.NewsApp
import entity.os.OS
import entity.phone.AndroidPhone
import entity.producer.Google
import entity.producer.Samsung
import entity.server.NewsPublisher
import entity.server.NotificationServer
import pattern.command.StartCommand
import pattern.command.StopCommand
import pattern.factory.AbstractPhoneFactory
import pattern.mediator.GoogleCloudMessaging
import pattern.mediator.NotificationSystem
import pattern.nullobject.NullCommand

fun main(args: Array<String>) {
    mediatorDemo()
    println()
    observerCommandDemo()
    println()
    templateMethodDemo()
    println()
    nullObjectDemo()
}

fun mediatorDemo() {
    println("*MEDIATOR DEMO*")
    val samsung = AndroidPhone(name = "Galaxy S8", producer = Samsung())
    val nexus = AndroidPhone(name = "Nexus", producer = Google())
    val notificationSystem: NotificationSystem = GoogleCloudMessaging()
    samsung.notificationSystem = notificationSystem
    nexus.notificationSystem = notificationSystem
    val firstServer = NotificationServer(notificationSystem)
    firstServer.notifyUsers("First server notification")
    samsung.checkNotifications()
    val secondServer = NotificationServer(notificationSystem)
    secondServer.notifyUsers("Second server notification")
    nexus.checkNotifications()
    samsung.checkNotifications()
}

fun observerCommandDemo() {
    println("*OBSERVER AND COMMAND DEMO*")
    val newsServer = NewsPublisher()
    val samsung = AndroidPhone(name = "Galaxy S8", producer = Samsung())
    val nexus = AndroidPhone(name = "Nexus", producer = Google())
    val firstApp = NewsApp(newsServer)
    samsung.installApp(firstApp)
    samsung.runCommand(StartCommand(firstApp))
    newsServer.notifyUsers("Good Weather!")
    newsServer.notifyUsers("Elections in Austria!")
    val secondApp = NewsApp(newsServer)
    nexus.installApp(secondApp)
    nexus.runCommand(StartCommand(secondApp))
    newsServer.notifyUsers("Earthquake!")
    samsung.runCommand(StopCommand(firstApp))
    newsServer.notifyUsers("Sunny day!")
    nexus.runCommand(StopCommand(secondApp))
}

fun templateMethodDemo(){
    println("*TEMPLATE METHOD ON ABSTRACT FACTORIES DEMO*")
    var factory : AbstractPhoneFactory = AbstractPhoneFactory.getFactory(OS.ANDROID)
    println(factory.createPhone())
    println(factory.createMarketApp())
    println()
    println("USING WINDOWS FACTORY")
    factory = AbstractPhoneFactory.getFactory(OS.WINDOWS)
    println(factory.createPhone())
    println(factory.createMarketApp())
    println()
    println("USING IOS FACTORY...")
    factory = AbstractPhoneFactory.getFactory(OS.IOS)
    println(factory.createPhone())
    println(factory.createMarketApp())
}

fun nullObjectDemo(){
    println("*NULL OBJECT DEMO*")
    val samsung = AndroidPhone(name = "Galaxy S8", producer = Samsung())
    println("running null command")
    samsung.runCommand(NullCommand())
    println("nothing done!")
}



