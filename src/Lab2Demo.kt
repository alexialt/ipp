import entity.app.GooglePlay
import entity.files.Element
import entity.files.File
import entity.files.Folder
import entity.message.Message
import entity.number.PhoneNumber
import entity.phone.Phone
import entity.phone.SymbianPhone
import entity.photo.PhotoType
import entity.user.Man
import entity.user.User
import entity.user.Woman
import pattern.adapter.OldPhoneAdapter
import pattern.builder.AppleBuilder
import pattern.builder.SamsungBuilder
import pattern.decorator.RabbitDecoratedElement
import pattern.decorator.SimpleDecoratedElement
import pattern.facade.SimpleCallCenter
import pattern.flyweight.FilesystemCache
import pattern.proxy.PhoneBooth
import java.util.*

fun main(args: Array<String>) {
    adapterDemo()
    bridgeDemo()
    compositeDecoratorDemo()
    privateDataDemo()
    proxyDemo()
    flyweightDemo()
    facadeDemo()

}

fun privateDataDemo() {
    println("*PRIVATE CLASS DEMO*")
    val iPhone = AppleBuilder()
            .createNewPhone()
            .buildMarketApp()
            .buildName("iPhone X")
            .buildWeight(200)
            .build()
    println(iPhone)
}

fun facadeDemo() {
    println("*FACADE DEMO*")
    SimpleCallCenter.getInstance().addUser("178632")
    SimpleCallCenter.getInstance().addUser("312567")
    SimpleCallCenter.getInstance().notifyAllUsers()
}

fun compositeDecoratorDemo() {
    println("*COMPOSITE + DECORATOR DEMO*")
    val iPhone = AppleBuilder()
            .createNewPhone()
            .buildMarketApp()
            .buildName("iPhone X")
            .buildWeight(200)
            .build()
    println(iPhone.fileSystem)
    var element : Element = SimpleDecoratedElement(iPhone.fileSystem)
    val subFolder = Folder("photos")
    subFolder.add(File("Sea.png", 2.25))
    subFolder.add(File("Sun.png", 1.75))
    subFolder.add(File("Mountain.png", 2.15))
    subFolder.add(File("Lake.png", 1.95))
    subFolder.add(File("Forest.png", 1.65))
    val meFolder = Folder("me")
    meFolder.add(File("about.txt", 0.15))
    meFolder.add(File("avatar.png", 1.05))
    element.add(subFolder)
    element = RabbitDecoratedElement(element)
    println(subFolder)
    subFolder.add(meFolder)
    println(SimpleDecoratedElement(subFolder))
    println(element)
}

fun proxyDemo() {
    println("*PROXY DEMO*")
    val proxy = PhoneBooth()
    proxy.call(PhoneNumber("+373", "22", "495211"))
    proxy.call(PhoneNumber("+7", "246", "317215114"))
}

fun flyweightDemo() {
    println("*FLYWEIGHT DEMO*")
    val flyweight = FilesystemCache()
    var element : Element = flyweight.getFilesystemElement("Jora",File::class.java)
    println(element)
    element = flyweight.getFilesystemElement("MyFolder",Folder::class.java)
    println(element)
    element = flyweight.getFilesystemElement("Jora",File::class.java)
    println(element)

}

fun bridgeDemo() {
    println("*BRIDGE DEMO*")
    var user: User = Woman("Maria")
    val iPhone = AppleBuilder()
            .createNewPhone()
            .buildMarketApp()
            .buildName("iPhone X")
            .buildWeight(200)
            .build()
    val galaxy = SamsungBuilder()
            .createNewPhone()
            .buildName("Galaxy S8+")
            .buildWeight(201)
            .build()
    user.takeSelfie()
    user.phone = iPhone
    user.takeSelfie()
    user = Man("John")
    user.takeSelfie()
    user.phone = galaxy
    user.takeSelfie()

}

fun adapterDemo() {
    println("*ADAPTER DEMO*")
    fun testPhone(phoneForTest: Phone) {
        phoneForTest.call(PhoneNumber("+373", "22", "493251"))
        phoneForTest.sendMessage(PhoneNumber("+373", "69", "683653"), Message(Date(), "Hey, Jora!", null))
        phoneForTest.makePhoto(PhotoType.FRONTAL)
        phoneForTest.makePhoto(PhotoType.REAR)
        phoneForTest.installApp(GooglePlay())
        println(phoneForTest)
    }

    val oldPhone = SymbianPhone()
    println("TESTING NEW PHONE...")
    var phone: Phone = SamsungBuilder()
            .createNewPhone()
            .buildWeight(100)
            .buildMarketApp()
            .buildName("X234")
            .build()
    testPhone(phone)
    println("TESTING OLD PHONE...")
    phone = OldPhoneAdapter(oldPhone)
    testPhone(phone)

}