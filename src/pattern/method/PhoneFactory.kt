package pattern.method

import entity.os.OS
import entity.phone.AndroidPhone
import entity.phone.IOSPhone
import entity.phone.WindowsPhone

class PhoneFactory private constructor(){
    companion object {
        fun createPhone(os: OS) = when (os) {
            OS.ANDROID -> AndroidPhone("J5")
            OS.WINDOWS -> WindowsPhone("X2")
            OS.IOS -> IOSPhone("X")
        }
    }
}