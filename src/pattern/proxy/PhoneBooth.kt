package pattern.proxy

import entity.number.PhoneNumber
import entity.phone.AndroidPhone
import entity.phone.Phone
import java.util.*

class PhoneBooth : EasyPhone {
    private var phone : Phone? = null
    override fun call(number: PhoneNumber) {
        println("ProxyPhone calling...")
        if (phone == null){
            println("ProxyPhone creates new Phone instance to redirect the call")
            phone = AndroidPhone("Galaxy "+(Random().nextInt()%10))
        }
        phone?.call(number)
    }
}