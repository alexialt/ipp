package pattern.proxy

import entity.number.PhoneNumber

interface EasyPhone {
    fun call(number : PhoneNumber)
}