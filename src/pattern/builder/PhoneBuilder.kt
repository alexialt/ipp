package pattern.builder

import entity.phone.BasePhone
import entity.phone.Phone

abstract class PhoneBuilder {
    protected var phone : BasePhone? = null

    abstract fun createNewPhone() : PhoneBuilder
    abstract fun buildName(name : String) : PhoneBuilder
    abstract fun buildWeight(weight : Int) : PhoneBuilder
    abstract fun buildMarketApp() : PhoneBuilder
    abstract fun build() : Phone
}