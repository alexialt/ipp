package pattern.builder

import entity.app.WindowsMarket
import entity.phone.Phone
import entity.phone.WindowsPhone
import entity.producer.Microsoft

class MicrosoftBuilder : PhoneBuilder(){


    override fun createNewPhone() = apply {
        this.phone= WindowsPhone()
        this.phone?.producer= Microsoft()
    }

    override fun buildName(name:String) = apply {
        this.phone?.name=name
    }

    override fun buildWeight(weight:Int) = apply {
        this.phone?.weight=weight
    }

    override fun buildMarketApp() = apply {
        this.phone?.installApp(WindowsMarket())
    }

    override fun build(): Phone {
        if (phone != null)
            return phone!!
        else
            throw RuntimeException("Cannot build phone!!! Make sure called createNewPhone() before build()")
    }

}