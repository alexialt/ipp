package pattern.builder

import entity.app.AppStore
import entity.phone.IOSPhone
import entity.phone.Phone

class AppleBuilder : PhoneBuilder() {


    override fun createNewPhone() = apply {
        this.phone = IOSPhone()
    }

    override fun buildName(name: String) = apply {
        this.phone?.name = name
    }

    override fun buildWeight(weight: Int) = apply {
        this.phone?.weight = weight
    }

    override fun buildMarketApp() = apply {
        this.phone?.installApp(AppStore())
    }

    override fun build(): Phone {
        if (phone != null)
            return phone!!
        else
            throw RuntimeException("Cannot build phone!!! Make sure called createNewPhone() before build()")
    }

}
