package pattern.builder

import entity.app.GooglePlay
import entity.phone.AndroidPhone
import entity.phone.Phone
import entity.producer.Samsung

class SamsungBuilder : PhoneBuilder() {


    override fun createNewPhone() = apply {
        this.phone = AndroidPhone()
        this.phone?.producer = Samsung()
    }

    override fun buildName(name: String) = apply {
        this.phone?.name = name
    }

    override fun buildWeight(weight: Int) = apply {
        this.phone?.weight = weight
    }

    override fun buildMarketApp() = apply {
        this.phone?.installApp(GooglePlay())
    }

    override fun build(): Phone {
        if (phone != null)
            return phone!!
        else
            throw RuntimeException("Cannot build phone!!! Make sure called createNewPhone() before build()")
    }
}