package pattern.singleton

class GlobalStats private constructor() {

    companion object {
        @Volatile private var stats : GlobalStats? = null
        private set
        fun getInstance() : GlobalStats?{
            if (stats == null)
                stats = GlobalStats()
            return stats
        }
    }
    var androidsSold: Int = 0
        private set
    var iosSold: Int = 0
        private set
    var windowsSold: Int = 0
        private set

    fun sellAndroid() = androidsSold++

    fun sellIOS() = iosSold++

    fun sellWindows() = windowsSold++

    override fun toString(): String {
        return "---stats---\n" +
                "iOS sold : $iosSold\n" +
                "Android sold : $androidsSold\n" +
                "Windows sold : $windowsSold\n" +
                "---*****---"
    }
}