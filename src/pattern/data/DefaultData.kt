package pattern.data

import com.sun.imageio.plugins.jpeg.JPEG

data class DefaultData(val screenLockTimeout : Long, val wallpaper : JPEG, val phoneName : String)