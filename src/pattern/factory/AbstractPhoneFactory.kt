package pattern.factory

import entity.app.MarketApp
import entity.os.OS
import entity.phone.Phone

abstract class AbstractPhoneFactory {

    abstract fun createPhone(): Phone
    abstract fun createMarketApp(): MarketApp

    companion object {
        fun getFactory(os: OS): AbstractPhoneFactory {
            return when (os) {
                OS.ANDROID -> AndroidPhoneFactory.getInstance()
                OS.IOS -> IOSPhoneFactory.getInstance()
                OS.WINDOWS -> WindowsPhoneFactory.getInstance()
            }
        }
    }

}