package pattern.factory

import entity.app.AppStore
import entity.app.MarketApp
import entity.phone.IOSPhone
import entity.phone.Phone
import entity.producer.Apple

class IOSPhoneFactory private constructor() : AbstractPhoneFactory() {
    override fun createPhone(): Phone {
        return IOSPhone("iPhone 7+")
    }

    override fun createMarketApp(): MarketApp {
        return AppStore()
    }

    companion object {
        @Volatile
        var factory: IOSPhoneFactory? = null

        fun getInstance(): IOSPhoneFactory {
            if (factory == null)
                factory = IOSPhoneFactory()
            return factory!!
        }
    }
}