package pattern.factory

import entity.app.GooglePlay
import entity.app.MarketApp
import entity.phone.AndroidPhone
import entity.phone.Phone

class AndroidPhoneFactory private constructor() : AbstractPhoneFactory() {
    override fun createPhone(): Phone {
        return AndroidPhone("Galaxy A7")
    }

    override fun createMarketApp(): MarketApp {
        return GooglePlay()
    }

    companion object {
        @Volatile
        var factory: AndroidPhoneFactory? = null

        fun getInstance(): AndroidPhoneFactory {
            if (factory == null)
                factory = AndroidPhoneFactory()
            return factory!!
        }
    }
}