package pattern.factory

import entity.app.MarketApp
import entity.app.WindowsMarket
import entity.phone.Phone
import entity.phone.WindowsPhone
import entity.producer.Microsoft

class WindowsPhoneFactory private constructor(): AbstractPhoneFactory() {
    override fun createPhone(): Phone {
        return WindowsPhone("Lumia X3",188, Microsoft())
    }

    override fun createMarketApp(): MarketApp {
        return WindowsMarket()
    }

    companion object {
        @Volatile
        var factory: WindowsPhoneFactory? = null

        fun getInstance(): WindowsPhoneFactory {
            if (factory == null)
                factory = WindowsPhoneFactory()
            return factory!!
        }
    }
}