package pattern.decorator

import entity.files.Element

class RabbitDecoratedElement(element : Element) : DecoratedElement(element) {
    override fun drawTop(): String {
        return "()()()()"
    }

    override fun drawBottom(): String {
        return "|_|-|_|"
    }
}