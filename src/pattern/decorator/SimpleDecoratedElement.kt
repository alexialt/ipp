package pattern.decorator

import entity.files.Element

class SimpleDecoratedElement(element : Element) : DecoratedElement(element) {
    override fun drawTop(): String {
        return "|---------|"
    }

    override fun drawBottom(): String {
        return "|_________|"
    }
}