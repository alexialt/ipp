package pattern.decorator

import entity.files.Element

abstract class DecoratedElement(val element: Element) : Element by element {
    override fun toString(): String {
        return drawTop() + "\n" + element.toString() + "\n" + drawBottom()
    }

    protected abstract fun drawTop(): String

    protected abstract fun drawBottom(): String
}