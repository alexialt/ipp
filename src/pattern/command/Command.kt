package pattern.command

interface Command {
    fun execute()
}