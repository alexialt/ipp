package pattern.command

import entity.app.App

class StartCommand(private val app: App) : Command {
    override fun execute() {
        app.run()
    }
}