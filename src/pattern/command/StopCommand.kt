package pattern.command

import entity.app.App

class StopCommand(private val app : App) : Command {
    override fun execute() {
        app.stop()
    }
}