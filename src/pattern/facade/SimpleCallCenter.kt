package pattern.facade

import entity.number.PhoneNumber
import entity.os.OS
import entity.phone.Phone
import pattern.method.PhoneFactory
import java.util.*

class SimpleCallCenter private constructor() : CallCenter {
    private val users: MutableList<PhoneNumber> = mutableListOf()
    private val phones: MutableList<Phone> = mutableListOf()
    private val rand = Random()

    companion object {
        @Volatile private var callCenter :SimpleCallCenter? = null
        fun getInstance(): SimpleCallCenter {
            if (callCenter == null)
                callCenter = SimpleCallCenter()
            return callCenter!!
        }
    }

    override fun call(number: String) {
        callNumber(PhoneNumber("+373", "22", number))
    }

    private fun callNumber(number: PhoneNumber) {
        println("Calling to "+number.number)
        val phone = if (phones.size > 0) phones[0] else run {
            println("Buying new phone")
            val newPhone = buyNewPhone()
            phones.add(newPhone)
            newPhone
        }
        println("Call center calling to " + number)
        phone.call(number)
        if (rand.nextBoolean() && phones.size > 0) {
            println("Phone was broken after call...")
            phones.removeAt(0)
        }
    }

    override fun addUser(number: String) {
        println("adding new user phone in database "+number)
        users.add(PhoneNumber("+373", "22", number))
    }

    override fun notifyAllUsers() {
        println("CallCenter notifying all users")
        for (user in users)
            callNumber(user)
    }

    override fun removeUser(number: String) {
        users.remove(PhoneNumber("+373", "22", number))
    }

    private fun buyNewPhone() = PhoneFactory.createPhone(OS.ANDROID)
}