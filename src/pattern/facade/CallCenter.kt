package pattern.facade

interface CallCenter {
    fun call(number : String)
    fun addUser(number : String)
    fun removeUser(number : String)
    fun notifyAllUsers()
}