package pattern.nullobject

import pattern.command.Command

class NullCommand : Command {
    override fun execute() {
        //doing nothing
    }
}