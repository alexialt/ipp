package pattern.mediator

interface NotificationSystem {
    fun pushNotification(str : String)
    fun getNotification(uid: String) : List<String>
}