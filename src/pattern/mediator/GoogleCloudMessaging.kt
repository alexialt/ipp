package pattern.mediator

import java.util.*
import java.util.concurrent.ConcurrentHashMap

class GoogleCloudMessaging : NotificationSystem {
    private val notifications = Collections.synchronizedList(mutableListOf<Notification>())
    private val history = ConcurrentHashMap<String, MutableSet<Notification>>()

    private companion object {
        private val timeout = 1000L
        private val limit = 10
    }


    @Synchronized
    override fun pushNotification(str: String) {
        cleanUp()
        notifications.add(Notification(msg = str))
        if (notifications.size > limit) {
            notifications.removeAt(0)
        }
    }

    private fun cleanUp() {
        notifications.removeIf { it.isExpired }
        val historyIterator = history.iterator()
        while (historyIterator.hasNext()) {
            val historyPair = historyIterator.next()
            historyPair.value.removeIf { it.isExpired }
        }
    }

    override fun getNotification(uid: String): List<String> {
        if (notifications.size == 0)
            return listOf()
        else {
            val sent = history[uid] ?: Collections.synchronizedSet(mutableSetOf<Notification>())
            val result = notifications.filter { !it.isExpired && !sent.contains(it) }
            history.put(uid,sent.apply { addAll(result) })
            return result.map { it.msg }
        }
    }

    private data class Notification(val time: Long = System.currentTimeMillis(), val msg: String) {
        val isExpired: Boolean
            get() = System.currentTimeMillis() - time > timeout
    }
}