package pattern.prototype

import entity.os.OS
import entity.phone.Phone

interface PhoneProtoFactory {
    fun getPhone(os : OS) : Phone
}