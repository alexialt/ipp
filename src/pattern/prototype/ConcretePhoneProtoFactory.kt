package pattern.prototype

import entity.os.OS
import entity.phone.AndroidPhone
import entity.phone.Phone
import entity.phone.WindowsPhone
import entity.producer.Samsung

class ConcretePhoneProtoFactory : PhoneProtoFactory{
    private val cache : MutableMap<OS, Phone> = mutableMapOf()

    init {
        cache.put(OS.ANDROID,AndroidPhone("Galaxy S8+",175, Samsung()))
        cache.put(OS.WINDOWS,WindowsPhone("Lumia X5",164))
        cache.put(OS.IOS,WindowsPhone("iPhone 5S",139))

    }

    override fun getPhone(os: OS) : Phone{
        val phone = cache[os]
        return phone?.clone() as Phone
    }
}