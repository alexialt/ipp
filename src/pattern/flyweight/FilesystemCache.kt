package pattern.flyweight

import entity.files.Element
import entity.files.File
import entity.files.Folder
import java.util.*

class FilesystemCache {
    private val map = mutableMapOf<Pair<String, Type>, Element>()
    private val rand = Random()
    fun getFilesystemElement(name: String, type : Class<out Element>): Element {
        val elType = when(type) {
            File::class.java -> Type.FILE
            else -> Type.FOLDER
        }
        return map[Pair(name,elType)] ?: run {
            println("Creating new $elType with name = $name")
            val element = if (elType == Type.FOLDER) Folder(name) else File(name,Math.abs(rand.nextInt() % 15).toDouble())
            map.put(Pair(name,elType),element)
            element
        }
    }

    private enum class Type {
        FILE, FOLDER
    }
}