package pattern.adapter

import com.sun.imageio.plugins.jpeg.JPEG
import entity.app.App
import entity.files.Element
import entity.files.File
import entity.message.Message
import entity.number.PhoneNumber
import entity.os.OS
import entity.phone.OldPhone
import entity.phone.Phone
import entity.phone.SymbianPhone
import entity.photo.Photo
import entity.photo.PhotoType
import entity.producer.Microsoft
import entity.producer.Producer
import pattern.command.Command
import pattern.data.DefaultData
import java.util.*
import javax.swing.text.html.HTML

class OldPhoneAdapter(private val oldPhone: OldPhone) : Phone {

    override val uid: String = UUID.randomUUID().toString()

    override fun runCommand(command: Command) {

    }

    override val defaultData: DefaultData
        get() = DefaultData(5000, JPEG(), oldPhone.model)

    override fun call(number: PhoneNumber) {
        oldPhone.makeCall(getPhoneNumber(number), false)
    }

    override fun installApp(app: App): Boolean {
        println("old phone does not support new app installation")
        return false
    }

    override fun makePhoto(type: PhotoType): Photo {
        return Photo(Date(), oldPhone.makePhoto())
    }

    override val fileSystem: Element
        get() = File("holder.inc", 1.5)

    override fun sendMessage(number: PhoneNumber, content: Message) {
        if (content.photo == null)
            oldPhone.sendSMS(getPhoneNumber(number), content.msg)
        else
            oldPhone.sendMMS(getPhoneNumber(number), HTML())
    }

    override fun clone(): Any {
        return SymbianPhone()
    }

    private fun getPhoneNumber(number: PhoneNumber): String {
        return number.countryCode + number.cityCode + number.number
    }

    override fun toString(): String {
        return "---\n" +
                "OldPhone (Using Adapter)\n" +
                "Producers: ${producer?.name ?: "No entity.producer specified"} \n" +
                "Model: ${name ?: "No Name Yet"} \n" +
                "Weight: ${weight ?: 0} \n" +
                "---"
    }

    override val producer: Producer?
        get() = Microsoft()
    override val weight: Int?
        get() = 100
    override val name: String?
        get() = oldPhone.model
    override val os: OS?
        get() = null
    override val apps: List<App>
        get() = listOf()
}